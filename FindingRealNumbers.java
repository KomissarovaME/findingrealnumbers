import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindingRealNumbers {
    public static void main(String[] args) {
        String identifiers ="";
        String text = "564.7 6768 78,6 -1.8 Kirill.89 достал.+уже908_дуться+.99";
        Pattern pattern = Pattern.compile("([-+]?\\d+[.,]\\d*)|([-+]?\\d*[.,]\\d+)");
        Matcher matcher = pattern.matcher(text);
        while(matcher.find()){
            identifiers += " " + matcher.group();
        }
        System.out.println(identifiers);
    }
}